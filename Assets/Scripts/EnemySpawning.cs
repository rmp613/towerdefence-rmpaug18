﻿using UnityEngine;
using System.Collections;

public class EnemySpawning : MonoBehaviour
{

    public Vector3 spawnerPosition;
    private Vector3 spawnRandom;
    public GameObject enemy;
    GameObject enemyClone;

    // Use this for initialization
    void Start()
    {
        spawnerPosition = GameObject.Find("SpawnZone").transform.position;

        spawnRandom.x = spawnerPosition.x + (Random.Range(-4, 4));
		spawnRandom.z = spawnerPosition.z + (Random.Range(-4, 4));
		spawnRandom.y = spawnerPosition.y;

        enemyClone = Instantiate(enemy, spawnRandom, Quaternion.identity) as GameObject;
        //enemyClone.AddComponent<Grid>();
    }

    // Update is called once per frame
    void Update()
    {



        //spawnRandom.x = spawnerPosition.x + (Random.Range(-5, 5));
        //spawnRandom.z = spawnerPosition.z + (Random.Range(-5, 5));

        ////GameObject basicEnemy = Resources.Load("BasicEnemy") as GameObject;

        ////Instantiate(basicEnemy, spawnRandom, Quaternion.identity);

        //enemyClone = Instantiate(enemy, spawnRandom, Quaternion.identity) as GameObject;

        if (Input.GetKeyDown(KeyCode.G))
        {
			spawnRandom.x = spawnerPosition.x + (Random.Range(-4, 4));
			spawnRandom.z = spawnerPosition.z + (Random.Range(-4, 4));
            spawnRandom.y = spawnerPosition.y;

            enemyClone = Instantiate(enemy, spawnRandom, Quaternion.identity) as GameObject;
        }


    }
}
