﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class GunFire : MonoBehaviour {
	GameObject ch;
	Animation gunAnim;
	// Use this for initialization
	void Start () {
		gunAnim = GetComponent<Animation> ();
		ch = GameObject.FindGameObjectWithTag ("Crosshair");
	}
	
	// Update is called once per frame
	void Update () {
		
		List<string> gunAnimNames = new List<string> ();
		foreach (AnimationState state in gunAnim) {
			gunAnimNames.Add (state.name);
		}
		if (Input.GetKeyDown(KeyCode.R) && GlobalAmmo.currentExtraAmmo > 0 && GlobalAmmo.currentAmmo < GlobalAmmo.maxClipSize) {
			//AudioSource reloadSound = GetComponent<AudioSource> ();
			//gunSound.Play ();
			Reload();
		}
		if (Input.GetButtonDown ("Fire1") && GlobalAmmo.currentAmmo > 0) {
			AudioSource gunSound = GetComponent<AudioSource> ();
			gunSound.Play ();
			gunAnim.Play ("GunShot");
			GlobalAmmo.currentAmmo--;
		}
		if(Input.GetButtonDown("Fire2")){
			gunAnim.Play ("ADS");
			ch.SetActive (false);
		}
		if(Input.GetButtonUp("Fire2")){
			gunAnim.Play ("HipFire");
			ch.SetActive (true);
		}
	}
	public void Reload(){
		GlobalAmmo.currentAmmo = GlobalAmmo.currentExtraAmmo + GlobalAmmo.currentAmmo;

		if(GlobalAmmo.currentAmmo >= GlobalAmmo.maxClipSize) {
			gunAnim.Play ("Reload");
			GlobalAmmo.currentExtraAmmo = GlobalAmmo.currentAmmo - GlobalAmmo.maxClipSize;
			GlobalAmmo.currentAmmo = GlobalAmmo.maxClipSize;
		}
	}
}
